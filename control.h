/*
 * edfinfo - read information from electricity meter (France)
 *
 * Copyright (C) 2015-2022, kaod.org
 *
 * This code is licensed under the GPL version 2 or later. See the
 * COPYING file in the top-level directory.
 */


#ifndef EDFINFO_SERVER_H
#define EDFINFO_SERVER_H

extern int control_fd;

int control_open();
int control_read(int sd);
int control_close(int sd);

#endif
