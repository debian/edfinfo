/*
 * edfinfo - read information from electricity meter (France)
 *
 * Copyright (C) 2015-2022, kaod.org
 *
 * This code is licensed under the GPL version 2 or later. See the
 * COPYING file in the top-level directory.
 */

#ifndef EDFINFO_EDFINFO_H
#define EDFINFO_EDFINFO_H

#include "config.h"

extern const char progname[];
extern const char version[];

#define __unused __attribute__((unused))

#endif
