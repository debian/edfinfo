/*
 * edfinfo - read information from electricity meter (France)
 *
 * Copyright (C) 2015-2022, kaod.org
 *
 * This code is licensed under the GPL version 2 or later. See the
 * COPYING file in the top-level directory.
 */

#ifndef EDFINFO_LOG_H
#define EDFINFO_LOG_H

#include <syslog.h>

#include "config.h"

#define ERROR(format, ...)				\
	if (LOG_ERR <= config.logpriority)		\
		__log(LOG_ERR, format, ##__VA_ARGS__);

#define WARN(format, ...)					\
	if (LOG_WARNING <= config.logpriority)			\
		__log(LOG_WARNING, format, ##__VA_ARGS__);

#define NOTICE(format, ...)					\
	if (LOG_NOTICE <= config.logpriority)			\
		__log(LOG_NOTICE, format, ##__VA_ARGS__);

#define INFO(format, ...)				\
	if (LOG_INFO <= config.logpriority)		\
		__log(LOG_INFO, format, ##__VA_ARGS__);

#define DEBUG(format, ...)					\
	if (LOG_DEBUG <= config.logpriority)			\
		__log(LOG_DEBUG, format, ##__VA_ARGS__);

extern int log_fd;

extern void __log(int priority, const  char *format, ...);
extern int log_open(const char *name);
extern int log_name_to_priority(const char *name);
extern const char *log_priority_to_name(int priority);

#endif
